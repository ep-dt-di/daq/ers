include_directories(/ers)

add_executable(test test.cxx)
target_link_libraries(test  ers pthread)

add_executable(receiver receiver.cxx)
target_link_libraries(receiver  ers pthread)
